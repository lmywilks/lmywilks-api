const OS          = require('os');
const PM2         = require('pm2');

const pm2 = new PM2.custom({
    secret_key   : "",
    public_key   : "",
    machine_name : require('os').hostname()
});

pm2.connect(function() {
    pm2.delete("lmywilks-api", function() {
        pm2.start({
            name               : "lmywilks-api",
            script             : __dirname + '/dist/server.js',
            exec_mode          : 'cluster',
            instances          : 0,
            max_memory_restart : "2024M",
            args               : ['--color'],
            node_args          : ["--harmony"],
            vizion             : false,
            watch              : true
        }, function(err, apps) {
            pm2.delete('pm2', function() {
                pm2.disconnect();
                process.exit(0);
            });
        });
    });
});