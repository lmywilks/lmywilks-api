import express              from "express";
import compression          from "compression"; // compresses request
import bodyParser           from "body-parser";
import dotenv               from "dotenv";
import mongoose             from "mongoose";
import expressValidator     from "express-validator";
import bluebird             from "bluebird";
import cors                 from "cors";

import { MONGODB_URI, TOKEN_SECRET, BASE_API } from "./utils";

// Load environment variables from .env file, where API keys and passwords are configured
dotenv.config({ path: ".env" });

class App {
    public app: any;

    constructor() {
        this.app = express();
        this.config();
        this.db();
        this.routes();
    }

    private config(): void {
        this.app.set("port", process.env.PORT || 3000);
        this.app.use(compression());
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({ extended: true }));
        this.app.use(expressValidator());
        this.app.use(cors());
    }

    private db(): void {
        (<any>mongoose).Promise = bluebird;

        mongoose.connect(MONGODB_URI, { useNewUrlParser: true })
            .then(() => { console.log("  Success to connect to MongoDB."); })
            .catch(err => { console.log("  MongoDB connection error. Please make sure MongoDB is running.\n" + err ); });
    }

    private routes(): void {
        this.app.get(
            `${ BASE_API }`, 
            (req: any, res: any, next: any) => {
                res.status(200).json({ message: 'Welcome.' }); 
            }
        );

        require("./routes")(this.app);
    }
}

export default new App().app;
