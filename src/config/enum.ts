export const UserModelEnum = {
    STATUS : {
        Active   : "A",
        Inactive : "I",
        Cancel   : "C",
        Deleted  : "D"
    },
    REQUIRE : {
        Username : "Username is required.",
        Password : "Password is required."
    },
    MESSAGE : {
        No_Username       : "Username ${username} is not exist.",
        Wrong_Password    : "Wrong Password for username ${username}.",
        Exist_User        : "Username ${username} is exist. Please use another one.",
        Not_Exist         : "User is not exist.",
        Update_PW_Fail    : "Wrong Password.",
        Update_PW_Success : "Update password successfully.",
        Same_Status       : "You have been ${role} already.",
        Update_Profile    : "Update profile successfully.",
        Not_Authorized    : "Not Authorized."
    }
};

export const GeneralEnum = {
    REQUIRE : (model, field) => { return `${ model } ${ field } is required.` },
    CREATE  : 'Create success.',
    UPDATE  : 'Update success.',
    DELETE  : 'Delete success.'
};

export const TransModelEnum = {
    TYPE : {
        Expense : 'E',
        Income  : 'I'
    }
};

export const RestaurantModelEnum = {

};

export const TodoModelEnum = {
    STATUS : {
        Open     : 'open',
        Done     : 'done',
        Rejected : 'rejected'
    }
};