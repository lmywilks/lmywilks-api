import { GeneralEnum } from "../config";

export class BaseController {
    private Model: any;

    constructor(model: any) {
        this.Model = model;
    }

    public ListAll = async (req: any, res: any, next: any) => {
        try {
            const results = await this.Model.find({});

            return res.status(200).json(results);
        } catch (e) {
            next(e);
        }
    };

    public ListByUser = async (req: any, res: any, next: any) => {
        try {
            const results = await this.Model.find({ user: req.params.user_id });

            return res.status(200).json(results);
        } catch (e) {
            next(e);
        }
    };

    public Retrieve = async (req: any, res: any, next: any) => {
        try {
            const result = await this.Model.findById(req.params.id);

            return res.status(200).json(result);
        } catch (e) {
            next(e);
        }
    };

    public Create = async (req: any, res: any, next: any) => {
        try {
            const result = new this.Model(req.body);

            await result.save();

            return res.status(201).json(result);
        } catch (e) {
            next(e);
        }
    };

    public Save = async (req: any, res: any, next:any) => {
        try {
            const result = await this.Model.findById(req.params.id);

            for (let key in req.body) {
                result[key] = req.body[key];
            }

            await result.save();

            return res.status(202).json({ message: GeneralEnum.UPDATE });
        } catch (e) {
            next(e);
        }
    };

    public Delete = async (req: any, res: any, next: any) => {
        try {
            const result = await this.Model.findByIdAndDelete(req.params.id);

            return res.status(200).json({ message: GeneralEnum.DELETE });
        } catch (e) {
            next(e);
        }
    };
}
