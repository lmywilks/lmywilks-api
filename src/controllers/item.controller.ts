import { Item } from "../models";

import { BaseController } from "./base.controller";

class ItemController extends BaseController {
    constructor() {
        super(Item);
    }

    public List = async (req: any, res: any, next: any) => {
        try {
            const items = await Item.find({
                user : req.params.user_id,
                todo : req.params.todo_id
            });

            return res.status(200).json(items);
        } catch (err) {
            next(err);
        }
    };
}

export default new ItemController();
