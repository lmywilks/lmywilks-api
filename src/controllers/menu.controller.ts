import { Restaurant, Menu } from "../models";
import { BaseController } from "./base.controller";

class MenuController extends BaseController {

    constructor() {
        super(Menu);
    }

    public List = async (req: any, res: any, next: any) => {
        try {
            const menu = await Menu.find({ restaurant: req.params.restaurant_id });

            return res.status(200).json(menu);
        } catch (err) {
            next(err);
        }
    }
}

export default new MenuController();