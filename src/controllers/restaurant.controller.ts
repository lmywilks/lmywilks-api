import { Restaurant, Menu } from "../models";
import { BaseController } from "./base.controller";
import { GeneralEnum } from "../config";

class RestaurantController extends BaseController {

    constructor() {
        super(Restaurant);
    }

    public ListName = async (req: any, res: any, next: any) => {
        try {
            const restaurants = await Restaurant.find({ });
            const names = restaurants.map(x => x.name);

            return res.status(200).json({ names });
        } catch (e) {
            next(e);
        }
    };

    public Retrieve = async (req: any, res: any, next: any) => {
        try {
            const restaurant = await Restaurant.findById(req.params.id);
            
            const menu = await Menu.find({ restaurant: req.params.id })

            let result: any = {};

            result._id = restaurant._id;
            result.description = restaurant.description;
            result.location = restaurant.location;
            result.phone = restaurant.phone;
            result.stars = restaurant.stars;
            result.images = restaurant.images;
            result.count = restaurant.count;
            result.menu = menu;

            return res.status(200).json(result);
        } catch (err) {
            next(err);
        }
    };

    public Delete = async (req: any, res: any, next: any) => {
        try {
            const menu = await Menu.deleteMany({ restaurant: req.params.id });

            const restaurant = await Restaurant.findByIdAndDelete(req.params.id);

            return res.status(200).json({ message: GeneralEnum.DELETE });
        } catch (err) {
            next(err);
        }
    };

    public Search = async (req: any, res: any, next: any) => {}
}

export default new RestaurantController();