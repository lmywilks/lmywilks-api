import Promise from "bluebird";
import { Todo, Item, TodoModel } from "../models";

import { BaseController } from "./base.controller";
import { GeneralEnum } from "../config";

class TodoController extends BaseController {
    constructor() {
        super(Todo);
    }

    public Create = async (req: any, res: any, next: any) => {
        try {
            
            const todo = await Todo.create({
                name : req.body.name,
                date : req.body.date,
                user : req.params.user_id
            });

            const lists = await Promise.map(req.body.list, async (item: any) => {
                const new_item = await Item.create({
                    title : item.title,
                    user  : req.params.user_id,
                    todo  : todo._id
                });

                return new_item;
            });

            res.status(201).json({ message: GeneralEnum.CREATE });

        } catch (err) {
            next(err);
        }
    }

    public Retrieve = async (req: any, res: any, next: any) => {
        try {
            const todo: TodoModel = await Todo.findById(req.params.id);

            const items = await Item.find({
                todo : req.params.id
            });

            let result: any = {};

            result.name = todo.name;
            result.date = todo.date;
            result._id = todo._id;
            result.status = todo.status;
            result.user = todo.user;
            result.list = items;

            return res.status(200).json(result);
        } catch (err) {
            next(err);
        }
    }

    public Search = async (req: any, res: any, next: any) => {
        try {
            const query_reg = new RegExp(req.params.query);
            const todos = await Todo.find({
                name: query_reg
            });

            return res.status(200).json(todos);
        } catch (err) {
            next(err);
        }
    }

    public Delete = async (req: any, res: any, next: any) => {
        try {            
            const list = await Item.deleteMany({
                user : req.params.user_id,
                todo : req.params.id
            });

            const todo = await Todo.findOneAndDelete({ 
                user : req.praams.user_id,
                _id  : req.params.id
            });

            return res.status(200).json({ message: GeneralEnum.DELETE });
        } catch (err) {
            next(err);
        }
    }
}

export default new TodoController();
