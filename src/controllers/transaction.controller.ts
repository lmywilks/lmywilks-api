import { Transaction } from "../models";

import { BaseController } from "./base.controller";

class TransactionController extends BaseController {
    constructor() {
        super(Transaction);
    }

    public ListByRange = async (req: any, res: any, next: any) => {
        try {
            const trans = await Transaction.find({
                user : req.params.user_id,
                date : { 
                    "$gt"  : new Date(parseInt(req.params.start) * 1000), 
                    "$lte" : new Date(parseInt(req.params.end) * 1000)
                } 
            });

            return res.status(200).json({ results: trans });
        } catch (e) {
            next(e);
        }
    };

    public ListByMonth = async (req: any, res: any, next: any) => {
    	try {
	    const start_month = parseInt(req.params.month);
	    const start_year = parseInt(req.params.year);
	    const end_month = start_month === 11 ? 0 : start_month + 1;
	    const end_year = start_month === 11 ? start_year + 1 : start_year;
	    const start_date = new Date(start_year, start_month, 1, 0, 0, 0);
	    const end_date = new Date(end_year, end_month, 1, 0, 0, 0, 0);

            const trans = await Transaction.find({
                date : {
                    $gte : start_date,
                    $lt  : end_date
                }
	        });

            let report = {
                expense : 0,
                income  : 0,
                total   : 0
            };

            trans.forEach(item => {
                switch (item.type) {
                    case 'E':
                        report.expense += item.amount;
                        report.total -= item.amount;
                        break;
                    case 'I':
                        report.income += item.amount;
                        report.total += item.amount;
                    default:
                        break;
                }
            });

            return res.status(200).json({ result: trans, report: report });
        } catch (e) {
            next(e);
        }
    };

}

export default new TransactionController();
