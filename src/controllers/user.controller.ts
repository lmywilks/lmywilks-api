import nodemailer   from "nodemailer";
import jwt          from "jsonwebtoken";

import { User } from "../models";
import { UserModelEnum, GeneralEnum } from "../config";
import { TOKEN_SECRET, ROLE_ADMIN } from "../utils";

class UserController {
    public Login = async (req: any, res: any, next: any) => {
        try {
            const user = await User.findOne({ username: req.body.username });

            if (!user) {
                return res.status(404).json({ message : UserModelEnum.MESSAGE.No_Username.replace('${username}', req.body.username) });
            }

            const isMatch = await user.comparePassword(req.body.password);

            if (!isMatch) {
                return res.status(401).json({ message : UserModelEnum.MESSAGE.Wrong_Password.replace('${username}', req.body.username) });
            }

            delete user.password;

            const token = jwt.sign({
                _id    : user._id,
                status : user.status,
                role   : user.role
            }, TOKEN_SECRET);

            return res.status(201).json({ id: user._id, token: token });
            
        } catch (e) {
            return next(e);
        }
    };

    public SignUp = async (req: any, res: any, next: any) => {
        try {
            const isExist = await User.findOne({ username: req.body.username });

            if (isExist) {
                return res.status(409).json({ message : UserModelEnum.MESSAGE.Exist_User.replace('${username}', req.body.username) });
            }

            const user = new User({
                username : req.body.username,
                password : req.body.password,
                profile  : req.body.profile
            });

            await user.save();

            delete user.password;

            const token = jwt.sign({
                _id    : user._id,
                status : user.status,
                role   : user.role
            }, TOKEN_SECRET);

            return res.status(201).json({
                id    : user._id,
                token : token
            });

        } catch (e) { 
            return next(e); 
        }
    };

    public Search = async (req: any, res: any, next: any) => {
        try {
            const users = await User.find({ username: new RegExp(req.params.query, 'ig') }, null);

            return res.status(200).json({ users: users });
        } catch (e) {
            next(e);
        }
    };

    public UpdateProfile = async (req: any, res: any, next: any) => {
        try {

            const user = req["lmywilks-user"];

            user.profile.name    = req.body.name || user.profile.name;
            user.profile.address = req.body.address || user.profile.address;
            user.profile.phone   = req.body.phone || user.profile.phone;
            user.profile.email   = req.body.email || user.profile.email;
            user.profile.website = req.body.website || user.profile.website;
            user.profile.avatar  = req.body.avatar || user.profile.avatar;

            await user.save();

            return res.status(202).json({ message: UserModelEnum.MESSAGE.Update_Profile });

        } catch (e) {
            return next(e);
        }
    };

    public UpdatePassword = async (req: any, res: any, next: any) => {
        try {

            const user = req["lmywilks-user"];

            const isMatch = await user.comparePassword(req.body.old_password);

            if (!isMatch) {
                return res.status(401).json({ message : UserModelEnum.MESSAGE.Update_PW_Fail });
            }

            user.password = req.body.new_password;

            await user.save();

            return res.status(201).json({ message: UserModelEnum.MESSAGE.Update_PW_Success });

        } catch (e) {
            return next(e);
        }
    };

    public UpdateStatus = async (req: any, res: any, next: any) => {
        try {

            const user = req["lmywilks-user"];

            user.status = req.body.status;

            await user.save();

            delete user.password;

            const token = jwt.sign({
                _id    : user._id,
                status : user.status,
                role   : user.role
            }, TOKEN_SECRET);

            return res.status(201).json({
                user  : user,
                token : token
            });

        } catch (e) {
            return next(e);
        }
    };

    public UpgradeRole = async (req: any, res: any, next: any) => {
        try {

            const user = req["lmywilks-user"];

            user.role = ROLE_ADMIN;

            await user.save();

            delete user.password;

            const token = jwt.sign({
                _id    : user._id,
                status : user.status,
                role   : user.role
            }, TOKEN_SECRET);

            return res.status(201).json({
                user  : user,
                token : token
            });

        } catch (e) {
            return next(e);
        }
    };

    public List = async (req: any, res: any, next: any) => {
        try {
            const users = await User.find({});

            return res.status(200).json({ users: users });
        } catch (e) {
            next(e);
        }
    };

    public Retrieve = async (req: any, res: any, next: any) => {
        try {
            const user = req["lmywilks-user"];

            delete user.password;

            return res.status(200).json({ user: user });
        } catch (e) {
            next(e);
        }
    };

    public Create = async (req: any, res: any, next: any) => {
        try {
            const user = new User(req.body);

            await user.save();

            return res.status(201).json({ user: user });
        } catch (e) {
            next(e);
        }
    };

    public Save = async (req: any, res: any, next: any) => {
        try {
            const user = await User.findById(req.params.id);

            for (let key in req.body) {
                user[key] = req.body[key];
            }

            await user.save();

            return res.status(202).json({ message: GeneralEnum.REQUIRE });
        } catch (e) {
            next(e);
        }
    };

    public Delete = async (req: any, res: any, next: any) => {
        try {
            const user = await User.findByIdAndDelete(req.params.id);

            return res.status(200).json({ message: GeneralEnum.DELETE });
        } catch (e) {
            next(e);
        }
    };
}

export default new UserController();
