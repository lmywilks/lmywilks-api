import mongoose from "mongoose";

import { GeneralEnum } from "../config";

export interface RestaurantModel extends mongoose.Document {
    name        : string,
    description : string,
    location    : string,
    phone       : string,
    stars       : number,
    images      : [string],
    count       : number
};

const restaurantSchema = new mongoose.Schema({

    name : {
        type      : String,
        trim      : true,
        lowercase : true,
        require   : GeneralEnum.REQUIRE('Restaurant', 'name')
    },

    description : String,

    location : String,

    phone : String,

    stars : {
        type    : Number,
        default : 0
    },

    images : [String],
    
    count : {
        type    : Number,
        default : 0
    }

}, { timestamps: true });

export const Restaurant = mongoose.model<RestaurantModel>('Restaurant', restaurantSchema);

export interface MenuModel extends mongoose.Document {
    name        : string,
    price       : number,
    count       : number,
    stars       : number,
    description : string,
    images      : [string],
    restaurant  : mongoose.Schema.Types.ObjectId
}

const menuSchema = new mongoose.Schema({

    name : {
        type      : String,
        trim      : true,
        lowercase : true,
        require   : GeneralEnum.REQUIRE('Menu', 'name')
    },

    description : String,

    price : String,

    stars : {
        type    : Number,
        default : 0
    },

    images : [String],
    
    count : {
        type    : Number,
        default : 0
    }

}, { timestamps: true });

export const Menu = mongoose.model<MenuModel>('Menu', menuSchema);
