import mongoose from "mongoose";

import { TodoModelEnum, GeneralEnum } from "../config";

export interface TodoModel extends mongoose.Document {
    name        : string,
    description : string,
    date        : Date,
    status      : string,
    user        : mongoose.Schema.Types.ObjectId
};

const todoSchema = new mongoose.Schema({
    
    name : {
        type     : String,
        trim     : true,
        required : GeneralEnum.REQUIRE('Todo', 'name')
    },

    description : String,

    date : {
        type     : Date,
        default  : new Date(),
        required : GeneralEnum.REQUIRE('todo', 'date')
    },

    status : {
        type     : String,
        default  : TodoModelEnum.STATUS.Open,
        enum     : [TodoModelEnum.STATUS.Open, TodoModelEnum.STATUS.Done, TodoModelEnum.STATUS.Rejected],
        required : GeneralEnum.REQUIRE('Todo', 'status')
    },

    user : {
        type    : mongoose.Schema.Types.ObjectId,
        ref     : 'User',
        require : GeneralEnum.REQUIRE('Todo', 'user')
    }

}, { timestamps: true });

export const Todo = mongoose.model<TodoModel>('Todo', todoSchema);

export interface ItemModel extends mongoose.Document {
    title       : string,
    status      : string,
    user        : mongoose.Schema.Types.ObjectId,
    todo        : mongoose.Schema.Types.ObjectId
};

const itemSchema = new mongoose.Schema({
    
    title : {
        type : String,
        required : GeneralEnum.REQUIRE('Item', 'title')
    },

    status : {
        type     : String,
        default  : TodoModelEnum.STATUS.Open,
        enum     : [TodoModelEnum.STATUS.Open, TodoModelEnum.STATUS.Done, TodoModelEnum.STATUS.Rejected],
        required : GeneralEnum.REQUIRE('Item', 'status')
    },

    user : {
        type    : mongoose.Schema.Types.ObjectId,
        ref     : 'User',
        require : GeneralEnum.REQUIRE('Todo', 'user')
    },

    todo : {
        type    : mongoose.Schema.Types.ObjectId,
        ref     : 'Todo',
        require : GeneralEnum.REQUIRE('Item', 'todo')
    }

}, { timestamps: true });

export const Item = mongoose.model<ItemModel>('Item', itemSchema);
