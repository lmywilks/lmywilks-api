import mongoose from "mongoose";

import { GeneralEnum, TransModelEnum } from "../config";

export interface TransactionModel extends mongoose.Document {
    name        : string,
    type        : string,
    description : string,
    date        : Date,
    amount      : number,
    user        : mongoose.Schema.Types.ObjectId,
    restaurant  : mongoose.Schema.Types.ObjectId
};

const transactionSchema = new mongoose.Schema({

    name : {
        type      : String,
        trim      : true,
        lowercase : true,
        require   : GeneralEnum.REQUIRE('Transaction', 'name')
    },

    type : {
        type    : String,
        enum    : [TransModelEnum.TYPE.Expense, TransModelEnum.TYPE.Income],
        default : TransModelEnum.TYPE.Expense,
        require : GeneralEnum.REQUIRE('Transaction', 'type')
    },

    description : String,

    date : {
        type    : Date,
        default : Date.now,
        require : GeneralEnum.REQUIRE('Transaction', 'date')
    },

    amount : {
        type    : Number,
        require : GeneralEnum.REQUIRE('Transaction', 'amount')
    },

    user : {
        type    : mongoose.Schema.Types.ObjectId,
        ref     : 'User',
        require : GeneralEnum.REQUIRE('Transaction', 'user')
    },

    restaurant : {
        type : mongoose.Schema.Types.ObjectId,
        ref  : 'Restaurant'
    }

}, { timestamps: true });

export const Transaction = mongoose.model<TransactionModel>('Transaction', transactionSchema);
