import bcrypt   from "bcrypt";
import crypto   from "crypto";
import mongoose from "mongoose";

import { UserModelEnum } from "../config";
import { ROLE_ADMIN, ROLE_USER } from "../utils";

export interface UserModel extends mongoose.Document {
    username : string,
    password : string,
    status   : string,
    role     : string,

    profile : {
        name    : string,
        address : string,
        phone   : string,
        email   : string,
        website : string,
        avatar  : string
    },

    comparePassword : comparePasswordFunction,
    gravatar        : (size: number) => string
};

type comparePasswordFunction = (candiatePassword: string) => Promise<boolean>;

const userSchema = new mongoose.Schema({

    username : {
        type     : String,
        unique   : true,
        trim     : true,
        required : UserModelEnum.REQUIRE.Username
    },

    password : {
        type     : String,
        required : UserModelEnum.REQUIRE.Password
    },

    status : {
        type    : String,
        default : UserModelEnum.STATUS.Active,
        enum    : Object.keys(UserModelEnum.STATUS).map(x => UserModelEnum.STATUS[x])
    },
    
    role : {
        type    : String,
        default : ROLE_USER,
        enum    : [ROLE_USER, ROLE_ADMIN]
    },

    profile : {
        name    : String,
        address : String,
        phone   : String,
        email   : String,
        website : String,
        avatar  : String
    }

}, { timestamps: true });

/**
 * Password hash middleware.
 */
userSchema.pre("save", function save(next) {
    const user = <any>this;

    if (!user.isModified("password")) { return next(); }

    bcrypt.genSalt(10, (err, salt) => {

        if (err) { return next(err); }

        bcrypt.hash(user.password, salt, (err: mongoose.Error, hash: any) => {

            if (err) { return next(err); }

            user.password = hash;

            next();

        });
    });
});

const comparePassword: comparePasswordFunction = function (candidatePassword) {
    return new Promise((resolve, reject) => {
        return bcrypt.compare(candidatePassword, this.password, (err: mongoose.Error, isMatch: boolean) => {
            if (err) reject(err);

            resolve(isMatch);
        });
    });
};

userSchema.methods.comparePassword = comparePassword;

/**
 * Helper method for getting user's gravatar.
 */
userSchema.methods.gravatar = function (size: number) {
    if (!size) { size = 200; }

    if (!this.username) { return `https://gravatar.com/avatar/?s=${ size }&d=retro`; }

    const md5 = crypto.createHash("md5").update(this.username).digest("hex");

    return `https://gravatar.com/avatar/${ md5 }?s=${ size }&d=retro`;
};

export const User = mongoose.model<UserModel>('User', userSchema);
// const User = mongoose.model("User", userSchema);

// export default User;
