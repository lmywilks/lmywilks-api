export = (app: any) => {
    require("./user.route")(app);
    require("./restaurant.route")(app);
    // require("./review.route")(app);
    // require("./transaction.route")(app);
    require("./todo.route")(app);
    require("./item.route")(app);
    require("./menu.route")(app);
}