import { ItemController } from "../controllers";

import { BASE_API, isAuthenticated, isAdmin, UserValidation } from "../utils";

export = (app: any) => {

    app.get(
        `${ BASE_API }/user/:user_id/todo/:todo_id/items`,
        [isAuthenticated, UserValidation],
        ItemController.List
    );

    app.get(
        `${ BASE_API }/user/:user_id/todo/:todo_id/item/:id`,
        [isAuthenticated, UserValidation],
        ItemController.Retrieve
    );

    app.post(
        `${ BASE_API }/user/:user_id/todo/:todo_id/item`,
        [isAuthenticated, UserValidation],
        ItemController.Create
    );

    app.put(
        `${ BASE_API }/user/:user_id/todo/:todo_id/item/:id`,
        [isAuthenticated, UserValidation],
        ItemController.Save
    );

    app.delete(
        `${ BASE_API }/user/:user_id/todo/:todo_id/item/:id`,
        [isAuthenticated, UserValidation],
        ItemController.Delete
    );
    
}
