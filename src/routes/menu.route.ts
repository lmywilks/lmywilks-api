import { MenuController } from "../controllers";

import { BASE_API, isAuthenticated, UserValidation } from "../utils";

export = (app: any) => {

    app.get(
        `${ BASE_API }/restaurant/:restaurant_id/menus`,
        [isAuthenticated],
        MenuController.List
    );

    app.get(
        `${ BASE_API }/restaurant/:restaurant_id/menu/:id`,
        [isAuthenticated],
        MenuController.Retrieve
    );

    app.post(
        `${ BASE_API }/restaurant/:restaurant_id/menu`,
        [isAuthenticated],
        MenuController.Create
    );

    app.put(
        `${ BASE_API }/restaurant/:restaurant_id/menu/:id`,
        [isAuthenticated],
        MenuController.Save
    );

    app.delete(
        `${ BASE_API }/restaurant/:restaurant_id/menu/:id`,
        [isAuthenticated],
        MenuController.Delete
    );

}