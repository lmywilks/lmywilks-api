import { RestaurantController } from "../controllers";

import { BASE_API, isAuthenticated, UserValidation } from "../utils";

export = (app: any) => {

    app.get(
        `${ BASE_API }/restaurants`,
        [isAuthenticated],
        RestaurantController.ListAll
    );

    app.get(
        `${ BASE_API }/restaurant/:id`,
        [isAuthenticated],
        RestaurantController.Retrieve
    );

    app.get(
        `${ BASE_API }/list/restaurant/name`,
        [isAuthenticated],
        RestaurantController.ListName
    );

    app.post(
        `${ BASE_API }/restaurant`,
        [isAuthenticated],
        RestaurantController.Create
    );

    app.put(
        `${ BASE_API }/restaurant/:id`,
        [isAuthenticated],
        RestaurantController.Save
    );

    app.delete(
        `${ BASE_API }/restaurant/:id`,
        [isAuthenticated],
        RestaurantController.Delete
    );

}