import { TodoController } from "../controllers";

import { BASE_API, isAuthenticated, isAdmin, UserValidation } from "../utils";

export = (app: any) => {

    app.get(
        `${ BASE_API }/user/:user_id/todos`,
        [isAuthenticated, UserValidation],
        TodoController.ListByUser
    );

    app.get(
        `${ BASE_API }/user/:user_id/todo/:id`,
        [isAuthenticated, UserValidation],
        TodoController.Retrieve
    );

    app.post(
        `${ BASE_API }/user/:user_id/todo`,
        [isAuthenticated, UserValidation],
        TodoController.Create
    );

    app.put(
        `${ BASE_API }/user/:user_id/todo/:id`,
        [isAuthenticated, UserValidation],
        TodoController.Save
    );

    app.delete(
        `${ BASE_API }/user/:user_id/todo/:id`,
        [isAuthenticated, UserValidation],
        TodoController.Delete
    );

    app.get(
        `${ BASE_API }/user/:user_id/todo/search/:query`,
        [isAuthenticated, UserValidation],
        TodoController.Search
    );
    
}
