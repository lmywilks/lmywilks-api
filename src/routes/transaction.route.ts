import { TransactionController } from "../controllers";

import { BASE_API, isAuthenticated, isAdmin, UserValidation } from "../utils";

export = (app: any) => {

    app.get(
        `${ BASE_API }/user/:user_id/transactions`,
        [isAuthenticated, UserValidation],
        TransactionController.ListByUser
    );

    app.get(
        `${ BASE_API }/user/:user_id/transactions/:start/:end`,
        [isAuthenticated, UserValidation],
        TransactionController.ListByRange
    );

    app.get(
        `${ BASE_API }/user/:user_id/transactions/year/:year/month/:month`,
        [isAuthenticated, UserValidation],
        TransactionController.ListByMonth
    );

    app.get(
        `${ BASE_API }/user/:user_id/transaction/:id`,
        [isAuthenticated, UserValidation],
        TransactionController.Retrieve
    );

    app.post(
        `${ BASE_API }/user/:user_id/transaction`,
        [isAuthenticated, UserValidation],
        TransactionController.Create
    );

    app.put(
        `${ BASE_API }/user/:user_id/transaction/:id`,
        [isAuthenticated, UserValidation],
        TransactionController.Save
    );

    app.delete(
        `${ BASE_API }/user/:user_id/transaction/:id`,
        [isAuthenticated, UserValidation],
        TransactionController.Delete
    );

    /**
     * ==========
     * Admin Route
     * ==========
    */

    app.get(
        `${ BASE_API }/list/transactions`,
        [isAuthenticated, isAdmin],
        TransactionController.ListAll
    );

}
