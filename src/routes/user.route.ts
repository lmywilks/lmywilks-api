import { UserController } from "../controllers";

import { BASE_API, isAuthenticated, isAdmin, UserValidation } from "../utils";

export = (app: any) => {
    app.post(
        `${ BASE_API }/login`, 
        UserController.Login
    );

    app.post(
        `${ BASE_API }/signup`, 
        UserController.SignUp
    );

    app.get(
        `${ BASE_API }/user/:user_id/search/:query`,
        [isAuthenticated, UserValidation], 
        UserController.Search
    );

    app.put(
        `${ BASE_API }/user/:user_id/update/password`, 
        [isAuthenticated, UserValidation], 
        UserController.UpdatePassword
    );

    app.put(
        `${ BASE_API }/user/:user_id/update/profile`, 
        [isAuthenticated, UserValidation], 
        UserController.UpdateProfile
    );

    app.post(
        `${ BASE_API }/user/:user_id/update/status`,
        [isAuthenticated, UserValidation], 
        UserController.UpdateStatus
    );

    /**
     * ==========
     * Admin Route
     * ==========
    */

    app.get(
        `${ BASE_API }/list/users`,
        [isAuthenticated, isAdmin],
        UserController.List
    );

    app.get(
        `${ BASE_API }/user/:id`,
        [isAuthenticated, isAdmin], 
        UserController.Retrieve
    );

    app.post(
        `${ BASE_API }/user`,
        [isAuthenticated, isAdmin], 
        UserController.Create
    );

    app.put(
        `${ BASE_API }/user/:id`,
        [isAuthenticated, isAdmin], 
        UserController.Save
    );

    app.delete(
        `${ BASE_API }/user/:id`,
        [isAuthenticated, isAdmin], 
        UserController.Delete
    );

    app.post(
        `${ BASE_API }/user/:user_id/upgrade/role`,
        [isAuthenticated, isAdmin],
        UserController.UpgradeRole
    );


}
