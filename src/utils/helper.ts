import { User } from "../models";
import { UserModelEnum } from "../config";

export const UserValidation = async (req: any, res: any, next: any) => {
    try {
        if (
            req["lmywilks-token"] && 
            req.params.user_id && 
            req["lmywilks-token"]._id === req.params.user_id
        ) {
            const user = await User.findById(req.params.user_id);

            if (!user) {
                return res.status(404).json({ message: UserModelEnum.MESSAGE.Not_Exist });
            }

            req["lmywilks-user"] = user;

            return next();
        }

        return res.status(401).json({ message : UserModelEnum.MESSAGE.Not_Authorized });

    } catch (e) {
        return next(e);
    }
};