import jwt from 'jsonwebtoken';

import { TOKEN_SECRET, ROLE_ADMIN, ROLE_USER } from "./secrets";
import { UserModelEnum } from "../config";

export const isAuthenticated = async (req: any, res: any, next: any) => {

    if (req && req.headers && req.headers["lmywilks-token"]) {

        try {
            const decode = await <any>jwt.verify(req.headers["lmywilks-token"], TOKEN_SECRET);

            if (
                decode["_id"] &&
                decode["status"] &&
                decode["status"] === UserModelEnum.STATUS.Active &&
                decode["role"] &&
                ( decode["role"] === ROLE_ADMIN || decode["role"] === ROLE_USER )
            ) {
                req["lmywilks-token"] = decode;

                return next();
            }

            return res.status(401).json({ message: UserModelEnum.MESSAGE.Not_Authorized });
        } catch(e) {
            return next(e);
        }
    }

    return res.status(401).json({ message: UserModelEnum.MESSAGE.Not_Authorized });
}

export const isAdmin = (req: any, res: any, next: any) => {
    if (
        req &&
        req["lmywilks-token"] &&
        req["lmywilks-token"]["_id"] &&
        req["lmywilks-token"]["status"] &&
        req["lmywilks-token"]["status"] === UserModelEnum.STATUS.Active &&
        req["lmywilks-token"]["role"] &&
        req["lmywilks-token"]["role"] === ROLE_ADMIN
    ) {
        return next();
    }

    return res.status(401).json({ message: UserModelEnum.MESSAGE.Not_Authorized });
}
