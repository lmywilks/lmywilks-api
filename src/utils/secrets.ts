import dotenv   from "dotenv";
import fs       from "fs";

if (fs.existsSync(".env")) {
    console.log('********************************************');
    console.log("  USing .env file to supply config environment variables.");
    dotenv.config({ path: ".env" });
}

export const LMYWILKS_ENV = process.env.NODE_ENV;
const prod = LMYWILKS_ENV === "production"; // Anything else is treated as 'dev'

// export const MONGODB_URI  = pord ? process.env["MONGODB_URI"] : '';
export const MONGODB_URI  = process.env["MONGODB_URI"];

export const TOKEN_SECRET = process.env["TOKEN_SECRET"];
export const BASE_API     = process.env["BASE_API"];
export const ROLE_ADMIN   = process.env["ROLE_ADMIN"];
export const ROLE_USER    = process.env["ROLE_USER"];

if (!MONGODB_URI) {
    console.log("No mongo connection string. Set MONGODB_URI environment variable.");
    process.exit(1);
}

if (!TOKEN_SECRET) {
    console.log("No client secret. Set TOKEN_SECRET environment variable.");
    process.exit(1);
}
